from django.urls import path

from . import views
from django.conf.urls import url

app_name = 'home'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('thankyou',views.thankyou, name='thankyou'),
]