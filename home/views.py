from django.shortcuts import render

# Create your views here.
def profile(request):
    return render(request, 'home/profile.html')


def thankyou(request):
    return render(request, 'home/thankyou.html')